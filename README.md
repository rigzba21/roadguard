# roadguard
A Rust binary for easy [road warrior](https://en.wikipedia.org/wiki/Road_warrior_(computing)) 
[Wireguard](https://www.wireguard.com/) VPN setup. 

⚠️ `roadguard` is currently undergoing significant updates and improvements. ⚠️

Example Usage:
```bash
# generate a wg0.conf for an Internet Gateway enabled interface, with
# IPv4/6 traffic forwarding and IP masquerade NAT nftables rules enabled.
roadguard generate-config --gateway-enabled --stdout
```

The `roadguard generate-config` command is used for advanced `[Interface]` configuration in `wg0.conf`:
```bash
Usage: roadguard generate-config [OPTIONS]

Options:
      --wg-address <WG_ADDRESS>    The Interface Address for WireGuard [default: 10.235.3.1]
  -g, --gateway-enabled            Setup as an internet gateway (Linux Only) - forwards IPv4/6 and IP masquerade NAT nftables rules; Default: true
      --relay-only                 Relay/Forward IPv6/6 traffic, non-gateway
      --dns <DNS>                  DNS - set the DNS entry in wg0.conf; Defaults to 1.1.1.1 [default: 1.1.1.1]
      --listen-port <LISTEN_PORT>  ListenPort - set the ListenPort in wg0.conf; Defaults to 51900 [default: 51900]
      --preup [<PREUP>...]         PreUp Commands - set one or more PreUp commands in wg0.conf;
      --postdown [<POSTDOWN>...]   PostDown commands - set one or more PostDown commands in wg0.conf
      --postup [<POSTUP>...]       PostUP commands - set one or more PostUp commands in wg0.conf
      --peer [<PEER>...]           Peer - set one or more WireGuard Peers; comma-separated. format: "PUBLIC-KEY,ENDPOINT:PORT,IP-ADDR/SUBNETMASK(comma-separated for multiple)"
  -s, --stdout                     Print wg0.conf to stdout; Default: false
      --write                      Write wg0.conf to /etc/wireguard/wg0.conf; Default: false
  -h, --help                       Print help
```

Advanced Example Usage:
```bash
# generate a wg0.conf for routing all internet traffic through peer
roadguard generate-config --stdout --peer "peer-public-key-base64-string,my.peer.public.ip:8989,0.0.0.0/0,::/0" 
```

## Quickstart
At this time `roadguard` only runs on Linux.

**Ubuntu Quickstart**

Install dependencies and useful tools:
```bash
# some dependencies
sudo apt-get install -y \
    wireguard \
    resolvconf \
    build-essential \
    qrencode

# rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source $HOME/.cargo/env
```

Install `roadguard`:
```bash
# clone the reop
git clone https://gitlab.com/rigzba21/roadguard.git
cd roadguard

# build release binary
cargo build --release

# install the binary
sudo install -m 755 target/release/roadguard /usr/local/bin/roadguard
```

Set up a Linux server as an Internet Gateway, with IPv4/6 traffic forwarding and IP masquerade NAT nftables rules enabled.:
```bash
# sudo may be required
roadguard generate-config --gateway-enabled --write
```

Bring up the `wg0` interface:
```bash
sudo wg-quick up wg0
```

Verify that it's up and running:
```bash
sudo wg show
```

At this point, you will have to set up a public endpoint for your WireGuard server. If you are running this on a cloud instance like EC2, you can either use the public IP address (or public DNS) or set up a DNS
record with a domain using your DNS Provider of choice. 

If you run the server on your network, such as a Raspberry Pi, you will need to configure your router to port-forward traffic on `51900`. Once you've configured port-forwarding to your server, you'll need to set up a DNS record that points towards your public IP address or set up a Dynamic DNS.


Once you've configured a public endpoint (Dynamic DNS, Public IP, etc.), you can now add `[Peer]` entries to your `wg0.conf`.

## Development Setup

# License

[MIT License](https://github.com/rigzba21/roadguard/blob/main/LICENSE)

