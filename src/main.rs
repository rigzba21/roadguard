use clap::{Parser, Subcommand};

use std::ops::Index;
use std::process::{Command, Stdio};
use std::fs::File;
use std::os::unix::fs::PermissionsExt;
use std::io::Write;
use std::process::exit;
use log::{info, error};

use nix::unistd::Uid;

use csv::ReaderBuilder;
use serde::Deserialize;

#[derive(Debug)]
pub enum WgInitErrors {
    WgDependenciesNotMet,
    FailedToGenPrivateKey,
    FailedToGenPublicKey,
    FailedToGetDefaultDevice,
    FailedToWriteWG0Conf,
}

#[derive(Debug)]
struct Interface {
    public_key: String,
    private_key: String,
    address: String,
    listen_port: String,
}

#[derive(Debug, Deserialize)]
struct Peer {
    public_key: String,
    endpoint: String,
    allowed_ips: Vec<String>,
}

#[derive(Debug)]
struct WG0Config {
    interface: Interface,
    dns: String,
    pre_up: Vec<String>,
    post_up: Vec<String>,
    post_down: Vec<String>,
    peers: Vec<Peer>,
}

/// roadguard - a CLI for setting up Road Warrior style WireGuard VPN endpoints
#[derive(Debug, Parser)] // requires `derive` feature
#[command(name = "roadguard")]
#[command(about = "a CLI for setting up Road Warrior style WireGuard VPN endpoints", long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Generate a wg0.conf for your WireGuard Interface
    //#[command(arg_required_else_help = true)]
    GenerateConfig{
        /// The Interface Address for WireGuard
        #[arg(long, default_value = "10.235.3.1", required = false)]
        wg_address: String,

        /// Setup as an internet gateway (Linux Only) - forwards IPv4/6 and IP masquerade NAT nftables rules; Default: true
        #[arg(short, long)]
        gateway_enabled: bool,

        /// Relay/Forward IPv6/6 traffic, non-gateway
        #[arg(long)]
        relay_only: bool,

        /// DNS - set the DNS entry in wg0.conf; Defaults to 1.1.1.1
        #[arg(long, default_value = "1.1.1.1", required = false)]
        dns: String,

        /// ListenPort - set the ListenPort in wg0.conf; Defaults to 51900
        #[arg(long, default_value = "51900", required = false)]
        listen_port: String,

        /// PreUp Commands - set one or more PreUp commands in wg0.conf;
        #[arg(long, num_args(0..))]
        preup: Vec<String>,

        /// PostDown commands - set one or more PostDown commands in wg0.conf
        #[arg(long, num_args(0..))]
        postdown: Vec<String>,

        /// PostUP commands - set one or more PostUp commands in wg0.conf
        #[arg(long, num_args(0..))]
        postup: Vec<String>,

        /// Peer - set one or more WireGuard Peers; comma-separated.
        /// format: "PUBLIC-KEY,ENDPOINT:PORT,IP-ADDR/SUBNETMASK(comma-separated for multiple)"
        #[arg(long, num_args(0..))]
        peer: Vec<String>,

        /// Print wg0.conf to stdout; Default: false
        #[arg(short, long)]
        stdout: bool,

        /// Write wg0.conf to /etc/wireguard/wg0.conf; Default: false
        #[arg(long)]
        write: bool,
    },
}

/// check for wg and wg-tools
fn check_wg_in_path() -> Result<bool, WgInitErrors> {
    let wg_version = Command::new("wg")
        .arg("--version")
        .output()
        .expect("wg --version command failed");

    let status_code = wg_version.status.code();

    match status_code {
        Some(0) => {
            info!("wg exists");
            return Ok(true)},
        _ => {
            let std_err = String::from_utf8(wg_version.stderr).unwrap();
            error!("{}", std_err);
            return Err(WgInitErrors::WgDependenciesNotMet)
        }
    };
}

/// check for wg-quick
fn check_wg_quick_in_path() -> Result<bool, WgInitErrors> {
    let wg_version = Command::new("which")
        .arg("wg-quick")
        .output()
        .expect("wg-quick command failed");

    let status_code = wg_version.status.code();

    match status_code {
        Some(0) => {
            info!("wg-quick exists");
            return Ok(true)
        },
        _ => {
            let std_err = String::from_utf8(wg_version.stderr).unwrap();
            error!("{}", std_err);
            return Err(WgInitErrors::WgDependenciesNotMet)
        }
    };
}

/// Execute the wg genkey command to return a generated private key
fn wg_genkey() -> Result<String, WgInitErrors> {
    let output = Command::new("wg")
        .arg("genkey")
        .output()
        .expect("failed to execute wg genkey");

    let status_code = output.status.code();

    match status_code {
        Some(0) => {
            let wg_private_key = String::from_utf8(output.stdout).unwrap()
                .replace("\n", "");
            info!("Generated private key");
            return Ok(wg_private_key);
        }
        _ => {
            let std_err = String::from_utf8(output.stderr).unwrap();
            error!("{:#?}", std_err);
            return Err(WgInitErrors::FailedToGenPrivateKey)
        }
    }

}

/// Execute the wg pubkey command to return the server public key
fn wg_pubkey(private_key: String) -> Result<String, WgInitErrors>{
    let echo_private_key = Command::new("echo")
        .arg(private_key)
        .stdout(Stdio::piped())
        .spawn()
        .expect("failed to echo private_key");

    let output = Command::new("wg")
        .arg("pubkey")
        .stdin(echo_private_key.stdout.unwrap())
        .output()
        .expect("failed to execute command \"wg pubkey\"");

    let status_code = output.status.code();

    match status_code {
        Some(0) => {
            let wg_public_key = String::from_utf8(output.stdout).unwrap()
                .replace("\n", "");
            info!("Generated public key");
            return Ok(wg_public_key);
        }
        _ => {
            let std_err = String::from_utf8(output.stderr).unwrap();
            error!("{:#?}", std_err);
            return Err(WgInitErrors::FailedToGenPrivateKey)
        }
    }
}

/// print wg0.conf to stdout
fn stdout_wg0_conf(wg0_conf: WG0Config) -> String {
    let mut interface_conf = format!(
"[Interface]
Address = {}/32
PrivateKey = {}
#For Reference - PublicKey = {}
ListenPort = {}
DNS = {}\n
", 
        wg0_conf.interface.address.replace("\n", ""), 
        wg0_conf.interface.private_key.replace("\n", ""), 
        wg0_conf.interface.public_key.replace("\n", ""),
        wg0_conf.interface.listen_port.replace("\n", ""), 
        wg0_conf.dns.replace("\n", ""));

    
    for preup_rule in wg0_conf.pre_up.iter() {
        let formatted_preup_rule = format!("PreUp = {}\n", preup_rule);
        interface_conf.push_str(&formatted_preup_rule);

    }

    interface_conf.push_str("\n");

    for postup_rule in wg0_conf.post_up.iter() {
        let formatted_postup_rule = format!("PostUp = {}\n", postup_rule);
        interface_conf.push_str(&formatted_postup_rule);
    };

    interface_conf.push_str("\n");

    for postdown_rule in wg0_conf.post_down.iter() {
        let formatted_postdown_rule = format!("\nPostDown = {}\n", postdown_rule);
        interface_conf.push_str(&formatted_postdown_rule);
    }

    interface_conf.push_str("\n");

    for peer in wg0_conf.peers.iter() {

        let mut allowed_ips = format!("");

        if peer.allowed_ips.len() == 1 {
            let allowed_ip = peer.allowed_ips.index(0);
            allowed_ips.push_str(&allowed_ip);
        }

        for ip in peer.allowed_ips.iter() {
            allowed_ips.push_str(&ip);
            allowed_ips.push_str(",");
        }

        // drop the last ',' in AllowedIPs
        allowed_ips.pop();


        let peer_block = format!(
"[Peer]
PublicKey = {}
AllowedIPs = {}
Endpoint = {}
",
        peer.public_key.replace("\n", ""),
        allowed_ips.replace("\n", ""),
        peer.endpoint.replace("\n", "")
        );

        interface_conf.push_str(&peer_block);
        interface_conf.push_str("\n");
    }

    return interface_conf
}

/// format Peers from String inputs from args
fn construct_peers(peers: Vec<String>) -> Vec<Peer> {
    let mut constructed_peers: Vec<Peer> = Vec::new();

    
    for peer in peers.iter() {
         
        // csv::Reader reader
        let mut rdr = ReaderBuilder::new()
        .has_headers(false)
        .from_reader(peer.as_bytes());

        for result in rdr.deserialize() {
            let constructed_peer: Peer = result.unwrap();
            constructed_peers.push(constructed_peer);
        }

    }

    return constructed_peers
}
/// write config to wg0.conf 
fn write_wg0_conf(config: String) -> std::io::Result<()> {
    // check if MacOS - write wg0.conf to /usr/local/etc/wireguard/wg0.conf
    if cfg!(target_os = "macos") {
        println!("writing wg0.conf to /usr/local/etc/wireguard/wg0.conf");

        // /usr/local/etc/wireguard should have 0755 permissions
        let mut file = File::create("/usr/local/etc/wireguard/wg0.conf")?;
        let metadata = file.metadata()?;
        let mut permissions = metadata.permissions();
    
        // umask 077
        permissions.set_mode(0o077); 
        assert_eq!(permissions.mode(), 0o077);
    
        file.write_all(config.as_bytes())?;
        return Ok(());
    }

    // windows - TODO
    if cfg!(windows) {
        println!("this is windows");

        return Ok(());
    } 
    
    if cfg!(unix) {
        if !Uid::effective().is_root() {
            panic!("You must run this executable with root permissions");
        }
        println!("writing wg0.conf to /etc/wireguard/wg0.conf");
        let mut file = File::create("/etc/wireguard/wg0.conf")?;
        let metadata = file.metadata()?;
        let mut permissions = metadata.permissions();
    
        // umask 077
        permissions.set_mode(0o077); 
        assert_eq!(permissions.mode(), 0o077);
    
        file.write_all(config.as_bytes())?;
        return Ok(());
    }

    Ok(())
}


fn main() {
    env_logger::init();
    let args = Cli::parse();

    let wg_exists = check_wg_in_path().unwrap();
    if !wg_exists {
        exit(1);
    }

    let wg_quick_exists = check_wg_quick_in_path().unwrap();
    if !wg_quick_exists {
        exit(1);
    }

    match args.command {
        Commands::GenerateConfig { 
            wg_address,
            gateway_enabled,
            relay_only,
            dns,
            listen_port,
            mut preup,
            mut postdown,
            mut postup,
            peer,
            stdout,
            write,
         } => {

            
            // if gateway_enabled  = true, only write to Linux/Unix hosts
            if cfg!(target_os = "macos") || cfg!(windows) {
                if (gateway_enabled || relay_only)  && write {
                    error!("Non-Linux/Unix OS detected; Internet Gateway or Relay currently only supports Linux");
                    exit(1);
                }

            } 

            // ensure that only one of Internet Gateway or Relay Only mode is selected
            if gateway_enabled && relay_only {
                error!("Internet Gateway AND Relay Only selected; Please only select one.");
                exit(1);
            }

            //generate private/public keypairs
            let _private_key = wg_genkey().unwrap();
            let _public_key = wg_pubkey(_private_key.clone()).unwrap();



            if gateway_enabled && !relay_only {          

                // IPv4/6 Forwarding
                let pre_up_ipv4_forwarding = r##"sysctl -w net.ipv4.ip_forward=1"##;
                let pre_up_ipv6_forwarding = r##"sysctl -w net.ipv6.conf.all.forwarding=1"##;

                preup.push(String::from(pre_up_ipv4_forwarding));
                preup.push(String::from(pre_up_ipv6_forwarding));

                // nftables IP masquerading and NAT rules
                let post_up_nftables_raw = r##"nft add table ip wireguard; nft add chain ip wireguard wireguard_chain {type nat hook postrouting priority srcnat\; policy accept\;}; nft add rule ip wireguard wireguard_chain counter packets 0 bytes 0 masquerade; nft add table ip6 wireguard; nft add chain ip6 wireguard wireguard_chain {type nat hook postrouting priority srcnat\; policy accept\;}; nft add rule ip6 wireguard wireguard_chain counter packets 0 bytes 0 masquerade"##;
                let post_down_nftables_raw = r##"nft delete table ip wireguard; nft delete table ip6 wireguard"##;

                postup.push(String::from(post_up_nftables_raw));
                postdown.push(String::from(post_down_nftables_raw));
            }

            if relay_only && !gateway_enabled {
                // IPv4/6 Forwarding
                let pre_up_ipv4_forwarding = r##"sysctl -w net.ipv4.ip_forward=1"##;
                let pre_up_ipv6_forwarding = r##"sysctl -w net.ipv6.conf.all.forwarding=1"##;

                preup.push(String::from(pre_up_ipv4_forwarding));
                preup.push(String::from(pre_up_ipv6_forwarding));   
            }


            match stdout {
                true => {
                    let _peers = construct_peers(peer.clone());

                    let _interface = Interface {
                        private_key: _private_key.clone(),
                        public_key: _public_key.clone(),
                        address: wg_address.clone(),
                        listen_port: listen_port.clone(),
                    };

                    let _wg0_conf = WG0Config {
                        interface: _interface,
                        dns: dns.clone(),
                        pre_up: preup.clone(),
                        post_down: postdown.clone(),
                        post_up: postup.clone(),
                        peers: _peers,
                    };
                    let wg0_conf = stdout_wg0_conf(_wg0_conf);

                    // print out wg0.conf
                    print!("{}", wg0_conf);

                    if write {

                        let result = write_wg0_conf(wg0_conf);
                        match result {
                            Ok(()) => {
                                print!("Successfully wrote wg0.conf");
                            },
                            Err(e) => {
                                println!("error writing wg0.conf: {}", e);
                                exit(1);
                            }
                        }
                    }
                },
                false => {
                    // just continue here
                }
            }

            // just write wg0.conf
            if write {
                let _peers = construct_peers(peer.clone());

                let _interface = Interface {
                    private_key: _private_key.clone(),
                    public_key: _public_key.clone(),
                    address: wg_address.clone(),
                    listen_port: listen_port.clone(),
                };

                let _wg0_conf = WG0Config {
                    interface: _interface,
                    dns: dns.clone(),
                    pre_up: preup.clone(),
                    post_down: postdown.clone(),
                    post_up: postup.clone(),
                    peers: _peers,
                };
                let wg0_conf = stdout_wg0_conf(_wg0_conf);
                let result = write_wg0_conf(wg0_conf);
                match result {
                    Ok(()) => {
                        print!("Successfully wrote wg0.conf");
                    },
                    Err(e) => {
                        println!("error writing wg0.conf: {}", e);
                        exit(1);
                    }
                }
            }

        },
    }

}


// RUST_LOG=info cargo test -- --nocapture
#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    fn logger_init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn test_verify_cli() {
        use clap::CommandFactory;
        Cli::command().debug_assert()
    }

    #[test]
    fn test_check_wg_in_path() {
        logger_init();
        let wg_exists = check_wg_in_path().unwrap();
        assert!(wg_exists);
    }

    #[test]
    fn test_check_wg_quick_in_path() {
        logger_init();
        let wg_exists = check_wg_quick_in_path().unwrap();
        assert!(wg_exists);
    }

    #[test]
    fn test_wg_genkey() {
        logger_init();
        let private_key = wg_genkey();
        assert!(private_key.is_ok());
    }

    #[test]
    fn test_wg_pubkey() {
        logger_init();
        let private_key = wg_genkey().unwrap();
        let public_key = wg_pubkey(private_key);
        assert!(public_key.is_ok());
    }

    #[test]
    fn test_wg0_conf() {
        logger_init();

        let mut pre_up: Vec<String> = Vec::new();
        let mut post_Up: Vec<String> = Vec::new();
        let mut post_down: Vec<String> = Vec::new();
        
        let wg0_conf = WG0Config {
            interface: Interface { 
                public_key: String::from("abcdef"), 
                private_key: String::from("ghijklm"), 
                address: String::from("1.2.3.4"), 
                listen_port: String::from("54321"),
            },
            dns: String::from("1.1.1.1"),
            pre_up: pre_up,
            post_up: post_Up,
            post_down: post_down,
        };
        
        let wg0_conf_string = stdout_wg0_conf(wg0_conf);
        info!("generated wg0.conf");
        print!("{}", wg0_conf_string);
        assert!(wg0_conf_string.contains("Address = 1.2.3.4/32"));
    }
}
